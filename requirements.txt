fastapi==0.110.0
SQLAlchemy==2.0.23
redis==5.0.2
pydantic==2.6.3
asyncpg==0.29.0
httpx==0.27.0
uvicorn==0.27.1
pytest==8.0.2
email-validator==2.1.1
argon2-cffi==23.1.0
PyJWT==2.8.0
python-dotenv==1.0.1
pydantic-settings==2.2.1
orjson==3.9.15
alembic==1.13.1
async-lru==2.0.4