from enum import Enum
from uuid import uuid4
from datetime import datetime

from sqlalchemy import ForeignKey
from sqlalchemy.orm import DeclarativeBase, mapped_column
from sqlalchemy.types import UUID, String, Numeric, Enum as SAEnum, Integer, Boolean, DateTime


class Base(DeclarativeBase):
    pass


class OrderStatus(str, Enum):
    PENDING = 'PENDING'
    PROCESSED = 'PROCESSED'
    REJECTED = 'REJECTED'
    EXPIRED = 'EXPIRED'


class UserStatus(str, Enum):
    REGISTERED = 'REGISTERED'
    ACTIVE = 'ACTIVE'
    BLOCKED = 'BLOCKED'
    INACTIVE = 'INACTIVE'


class Currency(str, Enum):
    USD = 'USD'
    EUR = 'EUR'


class User(Base):
    __tablename__ = 'users'

    id = mapped_column(UUID(as_uuid=True), primary_key=True, default=uuid4)
    username = mapped_column(String, nullable=False, index=True)
    email = mapped_column(String, nullable=False, index=True)
    password_hash = mapped_column(String, nullable=False)
    user_status = mapped_column(SAEnum(UserStatus), nullable=False, server_default='REGISTERED')
    dt_created = mapped_column(DateTime(timezone=True), nullable=False, default=datetime.now)
    dt_updated = mapped_column(DateTime(timezone=True), nullable=True)


class IceCream(Base):
    __tablename__ = 'ice_creams'

    id = mapped_column(UUID(as_uuid=True), primary_key=True, default=uuid4)
    # NOTE: Using simplified value, localisation should generally be considered
    flavor = mapped_column(String, nullable=False)
    # NOTE: Price and currency should be region specific, it is currently simplified
    price = mapped_column(Numeric(20, 2), nullable=False)
    currency = mapped_column(SAEnum(Currency), nullable=False, server_default='EUR')
    active = mapped_column(Boolean, nullable=False, server_default='true')
    dt_created = mapped_column(DateTime(timezone=True), default=datetime.now)
    dt_updated = mapped_column(DateTime(timezone=True), nullable=True)


class Order(Base):
    __tablename__ = 'orders'

    id = mapped_column(UUID(as_uuid=True), primary_key=True, default=uuid4)
    user_id = mapped_column(ForeignKey('users.id'), nullable=False)
    amount_to_pay = mapped_column(Numeric(20, 2), nullable=False)
    currency = mapped_column(SAEnum(Currency), nullable=False, server_default='EUR')
    status = mapped_column(SAEnum(OrderStatus), nullable=False, server_default='PENDING')


class OrderItem(Base):
    __tablename__ = 'order_items'

    id = mapped_column(UUID(as_uuid=True), primary_key=True, default=uuid4)
    order_id = mapped_column(ForeignKey('orders.id'), nullable=False)
    id_ice_cream = mapped_column(ForeignKey('ice_creams.id'), nullable=False)
    amount = mapped_column(Integer, nullable=False)
