import uuid
from enum import Enum
from uuid import uuid4

from sqlalchemy import ForeignKey
from sqlalchemy.orm import DeclarativeBase, mapped_column
from sqlalchemy.types import UUID, String, Numeric, Enum as SAEnum


class Base(DeclarativeBase):
    pass


class PaymentStatus(str, Enum):
    PENDING = 'PENDING'
    PROCESSED = 'PROCESSED'
    REJECTED = 'REJECTED'


class PaymentIssuerStatus(str, Enum):
    ACTIVE = 'ACTIVE'
    INACTIVE = 'INACTIVE'


class TransactionType(str, Enum):
    TEST = 'TEST'
    PRODUCTION = 'PRODUCTION'


class Currency(str, Enum):
    USD = 'USD'
    EUR = 'EUR'


class PaymentMerchant(Base):
    __tablename__ = 'payment_issuers'

    id = mapped_column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    name = mapped_column(String, nullable=False)
    status = mapped_column(SAEnum(PaymentIssuerStatus), nullable=False, server_default='ACTIVE')


class PaymentIssuerCredentials(Base):
    __tablename__ = 'payment_issuer_credentials'

    id = mapped_column(UUID(as_uuid=True), primary_key=True, default=uuid4)
    api_key = mapped_column(String, nullable=False, index=True)
    issuer = mapped_column(ForeignKey('payment_issuers.id'))
    credentials_type = mapped_column(SAEnum(TransactionType), nullable=False)


class Payment(Base):
    __tablename__ = 'payments'

    id = mapped_column(UUID(as_uuid=True), primary_key=True, default=uuid4)
    amount = mapped_column(Numeric(20, 2), nullable=False)
    id_payment_issuer = mapped_column(UUID(as_uuid=True), nullable=False)
    payment_type = mapped_column(SAEnum(TransactionType), nullable=False)
    payment_status = mapped_column(SAEnum(PaymentStatus), nullable=False, server_default='PENDING')
    issuer_callback = mapped_column(String, nullable=False)
