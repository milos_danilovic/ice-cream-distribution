import asyncio
from asyncpg.connection import Connection

from common.db.models.master import Base
from common.config import config
from common.db.migrations import base_env as base
from alembic import context


metadata = Base.metadata


def get_url():
    conf = config()
    return f'postgresql+asyncpg://' \
           f'{conf.master_db_user}:{conf.master_db_password}@' \
           f'{conf.master_db_host}:{conf.master_db_port}' \
           f'/{conf.master_db_database}'


def run_migrations_offline():
    global metadata
    return base.run_migrations_offline(metadata, get_url())


def do_run_migrations(connection: Connection) -> None:
    global metadata
    return base.do_run_migrations(connection, metadata)


async def run_migrations_online():
    global metadata
    return await base.run_migrations_online(get_url(), metadata)


if context.is_offline_mode():
    run_migrations_offline()
else:
    asyncio.run(run_migrations_online())
