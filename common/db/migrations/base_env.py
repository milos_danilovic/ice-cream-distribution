import sqlalchemy.exc
from alembic import context
from sqlalchemy import pool, text
from sqlalchemy.ext.asyncio import async_engine_from_config

from asyncpg.connection import Connection

from common.config import config as common_config

config = context.config


def run_migrations_offline(metadata, url):
    context.configure(
        url=url,
        target_metadata=metadata,
        literal_binds=True,
        dialect_opts={"paramstyle": "named"},
    )

    with context.begin_transaction():
        context.run_migrations()


async def check_database_exists(connection, url):
    db_name = url.split('/')[-1]
    try:
        await connection.execute(text('COMMIT'))
        await connection.execute(text(f'CREATE DATABASE {db_name}'))
    except sqlalchemy.exc.ProgrammingError as err:
        # Fix for missing database
        pass


def do_run_migrations(connection: Connection, metadata) -> None:
    context.configure(connection=connection, target_metadata=metadata)

    with context.begin_transaction():
        context.run_migrations()


async def run_migrations_online(url, metadata):
    configuration = config.get_section(config.config_ini_section)
    configuration["sqlalchemy.url"] = url
    base_config = dict(url='/'.join(url.split('/')[:-1]) + '/postgres')

    connectable_base = async_engine_from_config(
        base_config,
        prefix=''
    )

    async with connectable_base.connect() as connection:
        await check_database_exists(connection, url)

    connectable = async_engine_from_config(
        configuration,
        prefix="sqlalchemy.",
        poolclass=pool.NullPool,
    )

    async with connectable.connect() as connection:
        await connection.run_sync(do_run_migrations, metadata)

    await connectable.dispose()

