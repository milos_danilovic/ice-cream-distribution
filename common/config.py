from functools import lru_cache
from dotenv import load_dotenv
from pydantic_settings import BaseSettings, SettingsConfigDict


class Config(BaseSettings, case_sensitive=False):
    jwt_secret: str = ''
    jwt_algorithm: str = 'HS256'
    jwt_expire_days: int = 1

    master_db_user: str = 'user'
    master_db_password: str = 'password'
    master_db_host: str = 'localhost'
    master_db_port: int = 5432
    master_db_database: str = 'master'

    payments_db_user: str = 'user'
    payments_db_password: str = 'password'
    payments_db_host: str = 'localhost'
    payments_db_port: int = 5432
    payments_db_database: str = 'payments'

    redis_host: str = 'localhost'
    redis_port: int = 6379
    redis_password: str = 'redis_password'

    payment_api_key: str = 'fake_api_key'
    payment_api_url: str = 'http://payment'

    callback_url: str = 'http://wallet'
    callback_api_key: str = 'fake_api_key'

    model_config = SettingsConfigDict(env_file='.env')


@lru_cache(maxsize=1)
def config():
    return Config()
