from decimal import Decimal

from pydantic import BaseModel, AnyHttpUrl, UUID4
from .base import CamelBase


class TransactionReservationBase(BaseModel):
    id_user: str
    id_transaction: str
    order_data: dict


class ReservationWithAmount(TransactionReservationBase):
    amount_to_pay: Decimal


class TransactionReservation(ReservationWithAmount):
    callback: AnyHttpUrl


class PaymentProviderReservation(CamelBase):
    id_reservation: UUID4
    redirect_url: AnyHttpUrl
