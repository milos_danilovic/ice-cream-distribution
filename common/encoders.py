import json
from datetime import datetime
from uuid import UUID


class CustomJSONEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            return str(o)
        if isinstance(o, UUID):
            return str(o)
        return json.JSONEncoder.default(self, o)
