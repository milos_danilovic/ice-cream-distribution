import uvicorn

from fastapi import FastAPI, Depends
from common.schemas.payment import TransactionReservation

from payment.dependencies import db
from payment.dependencies.auth import auth, MerchantData

from payment.helpers import create_reservation


app = FastAPI()


@app.post('/charge', response_model=None)
async def charge(*,
                 data: TransactionReservation,
                 session: db.AsyncSession = Depends(db.session),
                 merchant: MerchantData = Depends(auth)):
    return await create_reservation(session, merchant, data)


if __name__ == '__main__':
    uvicorn.run('payment.main:app', port=8000, reload=True)
