import asyncio
from common.db.models import payments as models
from payment.dependencies import db
from sqlalchemy.exc import IntegrityError
from common.config import config


conf = config()


merchants = [
    models.PaymentMerchant(id='4ca36154-d978-4198-a5b6-7296ed45b171',
                           name='Test merchant',
                           status=models.PaymentIssuerStatus.ACTIVE)
]

merchant_credentials = [
    models.PaymentIssuerCredentials(id='a59b3283-117a-4970-91e8-e2feff0dd891',
                                    api_key=conf.payment_api_key,
                                    issuer='4ca36154-d978-4198-a5b6-7296ed45b171',
                                    credentials_type=models.TransactionType.TEST)
]


async def seed():
    async with db.session_manager.session() as sess:
        for merchant in merchants:
            try:
                sess.add(merchant)
                await sess.commit()
                await sess.refresh(merchant)
            except IntegrityError as err:
                await sess.rollback()
                continue
            except Exception as err:
                await sess.rollback()
                print(f'Error occurred while inserting merchant {merchant.name}: {err}')
                continue

        for merchant_cred in merchant_credentials:
            try:
                sess.add(merchant_cred)
                await sess.commit()
                await sess.refresh(merchant_cred)
            except IntegrityError as err:
                await sess.rollback()
                continue
            except Exception as err:
                await sess.rollback()
                print(f'Error occurred while inserting merchant credentials {merchant_cred.id}: {err}')
                continue



asyncio.run(seed())
