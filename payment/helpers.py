from uuid import UUID

from common.db.models import payments as models
from common.schemas.payment import TransactionReservation, PaymentProviderReservation
from common.config import config

from payment.dependencies.db import AsyncSession
from payment.dependencies.auth import MerchantData


def generate_redirect_url(id_transaction: UUID):
    conf = config()
    return f'{conf.payment_api_url}/{str(id_transaction)}'


async def create_reservation(session: AsyncSession,
                             merchant: MerchantData,
                             data: TransactionReservation):
    payment = models.Payment(amount=data.amount_to_pay,
                             id_payment_issuer=merchant.id_merchant,
                             payment_type=merchant.credentials_type,
                             issuer_callback=str(data.callback))

    session.add(payment)
    await session.commit()
    await session.refresh(payment)

    return PaymentProviderReservation(id_reservation=str(payment.id),
                                      redirect_url=generate_redirect_url(payment.id))
