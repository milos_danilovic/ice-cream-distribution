from sqlalchemy.ext.asyncio import AsyncSession

from common.db.session_manager import DatabaseSessionManager
from common.config import config


conf = config()
url = f'postgresql+asyncpg://' \
      f'{conf.payments_db_user}:{conf.payments_db_password}' \
      f'@{conf.payments_db_host}:{conf.payments_db_port}' \
      f'/{conf.payments_db_database}'

session_manager = DatabaseSessionManager(url)


async def session() -> AsyncSession:
    async with session_manager.session() as db_session:
        yield db_session
