from fastapi import Request, HTTPException
from fastapi.params import Depends

from pydantic import BaseModel
from pydantic.types import UUID4
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from common.config import config
from common.db.models import payments as models

from payment.dependencies import db


class MerchantData(BaseModel):
    id_merchant: UUID4
    credentials_type: models.TransactionType


class AuthMerchant(Depends):
    @staticmethod
    async def get_merchant_data(session: AsyncSession, api_key: str):
        query = select(models.PaymentIssuerCredentials).where(models.PaymentIssuerCredentials.api_key == api_key)
        return (await session.scalars(query)).one_or_none()

    async def __call__(self,
                       request: Request,
                       session: AsyncSession = Depends(db.session)):
        api_key = request.headers.get('X-API-Key')
        if not api_key:
            raise HTTPException(status_code=403, detail='Forbidden')

        try:
            merchant_credentials: models.PaymentIssuerCredentials = await self.get_merchant_data(session, api_key)
        except Exception as err:
            raise HTTPException(status_code=400, detail='Service not available (DBERR)')

        return MerchantData(id_merchant=merchant_credentials.issuer,
                            credentials_type=merchant_credentials.credentials_type)


auth = AuthMerchant()
