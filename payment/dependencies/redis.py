from typing import AsyncIterator

import orjson
import redis.asyncio as redis
from redis.asyncio import Redis
from common.config import config
from async_lru import alru_cache


@alru_cache(maxsize=1)
async def pool() -> redis.ConnectionPool:
    conf = config()
    return redis.ConnectionPool.from_url(f'redis://:{conf.redis_password}@{conf.redis_host}:{conf.redis_port}')


async def session() -> AsyncIterator[Redis]:
    session = redis.Redis(connection_pool=await pool())
    try:
        yield session
    except Exception as err:
        await session.aclose()
        raise


async def json_get(conn: Redis, key: str):
    data = await conn.get(key)
    return orjson.loads(data) if data else None


async def json_set(conn: Redis, key: str, data: dict, ex=None):
    await conn.set(key, orjson.dumps(data), ex=ex)
    return data
