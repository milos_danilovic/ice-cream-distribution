import asyncio
import subprocess
from app.seed import seed as master_seed
from payment.seed import seed as payment_seed


subprocess.call(['alembic', '-c', 'common/db/migrations/master/alembic.ini', 'upgrade', 'head'])
subprocess.call(['alembic', '-c', 'common/db/migrations/payments/alembic.ini', 'upgrade', 'head'])

asyncio.run(master_seed())
asyncio.run(payment_seed())
