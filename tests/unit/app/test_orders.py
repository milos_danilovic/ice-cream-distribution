from datetime import datetime, timedelta, UTC
from uuid import uuid4


from unittest import IsolatedAsyncioTestCase
from unittest.mock import AsyncMock, patch
from httpx import AsyncClient

from fastapi.testclient import TestClient

from app.main import app
from app.dependencies.auth import auth, JWTUser
from app.orders import schemas
from common.db.models.master import Currency, IceCream


class TestCart(IsolatedAsyncioTestCase):
    @property
    def client(self):
        app.dependency_overrides = {}
        return AsyncClient(app=app, base_url='http://test')

    @property
    def mock_cart(self):
        return schemas.Cart(items=[])

    @property
    def mock_user(self):
        return JWTUser(id=uuid4(),
                       sub=str(uuid4()),
                       exp=datetime.now(tz=UTC) + timedelta(days=5),
                       username='test',
                       email='test@test.com',
                       user_status='ACTIVE',
                       dt_created=datetime.now())

    async def return_mock_user(self):
        return self.mock_user

    @property
    def patched_auth_client(self):
        app.dependency_overrides[auth] = self.return_mock_user
        return AsyncClient(app=app, base_url='http://test')

    @property
    def mock_ice_creams(self):
        dt_created = datetime.now()
        return [IceCream(id='e76fa304-5049-4c5c-8238-c00b2eee1405',
                         flavor='Chocolate',
                         price=2.99,
                         currency=Currency.USD,
                         active=True,
                         dt_created=dt_created),
                IceCream(id='4820417a-ecbd-4b30-8013-331af424f72d',
                         flavor='Vanilla',
                         price=2.99,
                         currency=Currency.USD,
                         active=True,
                         dt_created=dt_created)]

    @patch('app.orders.helpers.get_ice_creams', new_callable=AsyncMock)
    async def test_01_get_ice_creams(self, get_ice_creams):
        get_ice_creams.return_value = self.mock_ice_creams
        async with self.client as client:
            response = await client.get('/orders/items')
            assert response.status_code == 200

    @patch('app.orders.helpers.get_user_cart', new_callable=AsyncMock)
    async def test_01_get_cart_base(self, get_user_cart):
        get_user_cart.return_value = self.mock_cart
        async with self.patched_auth_client as client:
            response = await client.get('/orders/cart')
            assert response.status_code == 200
            assert response.json()

    @patch('app.orders.helpers.save_cart', new_callable=AsyncMock)
    @patch('app.orders.helpers.get_user_cart', new_callable=AsyncMock)
    @patch('app.orders.helpers.get_single_ice_cream', new_callable=AsyncMock)
    async def test_02_add_to_cart(self, single_ice_cream, get_user_cart, save_cart):
        single_ice_cream.return_value = self.mock_ice_creams[0]
        get_user_cart.return_value = self.mock_cart
        save_cart.return_value = None
        amount = 10

        async with self.patched_auth_client as client:
            response = await client.post('/orders/cart/add', json=dict(id_ice_cream=self.mock_ice_creams[0].id,
                                                                       amount=amount))
            assert response.status_code == 200
            response_data = response.json()
            assert response_data
            assert response_data['items'][0]['idIceCream'] == self.mock_ice_creams[0].id
            assert response_data['items'][0]['amount'] == amount
