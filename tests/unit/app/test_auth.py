import jwt

from uuid import uuid4
from functools import lru_cache
from dataclasses import dataclass
from datetime import datetime

from unittest import IsolatedAsyncioTestCase
from unittest.mock import AsyncMock, patch

from httpx import AsyncClient
from argon2 import PasswordHasher

from common.db.models import master as models
from common.config import Config

from app.main import app
from app.auth import helpers
from app.auth import schemas
from app.dependencies.auth import AuthUser


@dataclass
class TestData:
    username: str = 'test_username'
    email: str = 'test@test.com'
    password: str = 'test_password'
    fake_password: str = 'fake_password'
    test_token: str = 'test_token'
    jwt_secret: str = 'secret'
    other_jwt_secret: str = 'other_secret'


class TestAuth(IsolatedAsyncioTestCase):
    @staticmethod
    @lru_cache(maxsize=1)
    def hasher() -> PasswordHasher:
        return PasswordHasher()

    @property
    def client(self):
        return AsyncClient(app=app, base_url='http://test')

    @property
    def create_password(self):
        return TestAuth.hasher().hash(TestData.password)

    @property
    @lru_cache(maxsize=1)
    def mock_user(self):
        return models.User(id=uuid4(),
                           username=TestData.username,
                           email='test@test.com',
                           user_status=models.UserStatus.ACTIVE,
                           dt_created=datetime.now(),
                           password_hash=self.create_password)

    @patch('app.auth.helpers.create_access_token', new_callable=AsyncMock)
    @patch('app.auth.helpers.create_user', new_callable=AsyncMock)
    @patch('app.auth.helpers.get_user', new_callable=AsyncMock)
    async def test_01_register_positive(self, get_user, create_user, create_access_token):
        get_user.return_value = None
        create_user.return_value = self.mock_user
        create_access_token.return_value = TestData.test_token

        async with self.client as ac:
            response = await ac.post('auth/register', json=dict(username=TestData.username,
                                                                password=TestData.password,
                                                                email=TestData.email))
            assert response.status_code == 200
            parsed_response = response.json()
            assert parsed_response
            assert parsed_response.get('accessToken')
            assert parsed_response.get('accessToken') == TestData.test_token

    @patch('app.auth.helpers.create_access_token', new_callable=AsyncMock)
    @patch('app.auth.helpers.create_user', new_callable=AsyncMock)
    @patch('app.auth.helpers.get_user', new_callable=AsyncMock)
    async def test_011_register_user_exists(self, get_user, create_user, create_access_token):
        get_user.return_value = self.mock_user
        create_user.return_value = None
        create_access_token.return_value = None

        async with self.client as ac:
            response = await ac.post('auth/register', json=dict(username=TestData.username,
                                                                password=TestData.password,
                                                                email=TestData.email))
            assert response.status_code == 400
            assert response.json()['detail'] == 'User already exists'

    @patch('app.auth.helpers.create_access_token', new_callable=AsyncMock)
    @patch('app.auth.helpers.check_password', new_callable=AsyncMock)
    @patch('app.auth.helpers.get_user', new_callable=AsyncMock)
    async def test_02_login_positive(self, get_user, check_password, create_access_token):
        get_user.return_value = self.mock_user
        check_password.return_value = True
        create_access_token.return_value = TestData.test_token

        async with self.client as ac:
            response = await ac.post('auth/login', json=dict(username=TestData.username,
                                                             password=TestData.password,
                                                             email=TestData.email))
            assert response.status_code == 200
            parsed_response = response.json()
            assert parsed_response
            assert parsed_response.get('accessToken')
            assert parsed_response.get('accessToken') == 'test_token'

    @patch('app.auth.helpers.create_access_token', new_callable=AsyncMock)
    @patch('app.auth.helpers.check_password', new_callable=AsyncMock)
    @patch('app.auth.helpers.get_user', new_callable=AsyncMock)
    async def test_02_login_user_not_found(self, get_user, check_password, create_access_token):
        get_user.return_value = None
        check_password.return_value = True
        create_access_token.return_value = TestData.test_token

        async with self.client as ac:
            response = await ac.post('auth/login', json=dict(username=TestData.username,
                                                             password=TestData.password,
                                                             email=TestData.email))
            assert response.status_code == 404
            parsed_response = response.json()
            assert parsed_response['detail'] == 'User not found'

    async def test_03_check_password_correct(self):
        is_correct = await helpers.check_password(self.mock_user,
                                                  schemas.UserBase(username=TestData.username,
                                                                   password=TestData.password))
        assert is_correct is True

    async def test_04_check_password_incorrect(self):
        is_correct = await helpers.check_password(self.mock_user,
                                                  schemas.UserBase(username=TestData.username,
                                                                   password=TestData.fake_password))
        assert is_correct is False

    async def test_05_generate_hash(self):
        password_hash = await helpers.generate_password_hash(schemas.UserBase(username=TestData.username,
                                                                              password=TestData.password))

        assert self.hasher().verify(password_hash, TestData.password)

    async def test_06_create_access_token(self):
        auth = AuthUser(conf=Config(jwt_secret=TestData.jwt_secret,
                                    jwt_algorithm='HS256'))
        access_token = await auth.create_access_token(self.mock_user)
        assert access_token is not None

        assert jwt.decode(access_token,
                          key=TestData.jwt_secret,
                          algorithms=['HS256'])

    @patch('app.dependencies.auth.auth', AuthUser(conf=Config(jwt_secret=TestData.other_jwt_secret,
                                                              jwt_algorithm='HS256')))
    async def test_07_invalid_access_token(self):
        auth = AuthUser(conf=Config(jwt_secret=TestData.other_jwt_secret,
                                    jwt_algorithm='HS256'))
        access_token = await auth.create_access_token(self.mock_user)
        assert access_token is not None
        try:
            jwt.decode(access_token,
                       key=TestData.jwt_secret,
                       algorithms=['HS256'])
        except Exception as err:
            assert isinstance(err, jwt.exceptions.InvalidSignatureError)
