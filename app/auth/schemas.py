from typing import Annotated
from datetime import datetime

from pydantic import Field, StringConstraints, SecretStr, EmailStr
from pydantic.types import UUID4

from common.schemas.base import CamelBase
from common.db.models import master as models


class UserBase(CamelBase):
    username: Annotated[str, StringConstraints(min_length=3)]
    password: Annotated[SecretStr, StringConstraints(min_length=8)]


class UserRegistration(UserBase):
    email: EmailStr


class AccessTokenResponse(CamelBase):
    access_token: str
