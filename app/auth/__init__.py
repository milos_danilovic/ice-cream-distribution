from fastapi import APIRouter

from . import routes


router = APIRouter(tags=['Auth'])


router.add_api_route('/register', routes.register, methods=['POST'])
router.add_api_route('/login', routes.login, methods=['POST'])
