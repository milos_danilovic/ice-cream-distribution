import asyncio
import jwt

from functools import lru_cache, partial
from datetime import datetime, timedelta, timezone

from sqlalchemy import select
from sqlalchemy import or_
from sqlalchemy.exc import IntegrityError
from sqlalchemy.ext.asyncio import AsyncSession

from fastapi import HTTPException

from argon2 import PasswordHasher
from argon2.exceptions import VerifyMismatchError
from async_lru import alru_cache

from app.dependencies.auth import auth
from common.db.models import master as models

from . import schemas


@alru_cache(maxsize=1)
async def get_hasher() -> PasswordHasher:
    return PasswordHasher()


async def get_user(session: AsyncSession,
                   data: schemas.UserBase | schemas.UserRegistration,
                   check_email=False):
    statement = select(models.User).where(models.User.username == data.username)
    if check_email:
        statement = select(models.User) \
            .where(or_(models.User.username == data.username,
                       models.User.email == data.email))

    return (await session.scalars(statement)).one_or_none()


async def create_user(session: AsyncSession,
                      data: schemas.UserRegistration) -> models.User:
    password_hash = await generate_password_hash(data)
    dt_now = datetime.now()

    db_user = models.User(username=data.username,
                          email=data.email,
                          password_hash=password_hash,
                          user_status=models.UserStatus.ACTIVE,
                          dt_created=dt_now,
                          dt_updated=dt_now)
    session.add(db_user)
    await session.commit()
    await session.refresh(db_user)

    return db_user


async def create_access_token(user: models.User):
    return await auth.create_access_token(user)


async def check_password(user: models.User,
                         request_data: schemas.UserBase):
    hasher = await get_hasher()
    try:
        return await asyncio.get_event_loop().run_in_executor(None,
                                                              partial(hasher.verify,
                                                                      user.password_hash,
                                                                      request_data.password.get_secret_value()))
    except VerifyMismatchError:
        return False


async def generate_password_hash(data: schemas.UserBase):
    hasher = await get_hasher()
    return await asyncio.get_event_loop().run_in_executor(None, hasher.hash, data.password.get_secret_value())
