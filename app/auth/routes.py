from fastapi import HTTPException, Depends

from sqlalchemy.ext.asyncio import AsyncSession

from app.dependencies import db

from . import schemas
from . import helpers


async def register(*,
                   session: AsyncSession = Depends(db.session),
                   data: schemas.UserRegistration):
    existing_user = await helpers.get_user(session, data, check_email=True)
    if existing_user:
        raise HTTPException(status_code=400, detail='User already exists')

    new_user = await helpers.create_user(session, data)

    access_token = await helpers.create_access_token(new_user)
    return schemas.AccessTokenResponse(access_token=access_token)


async def login(*,
                session: AsyncSession = Depends(db.session),
                data: schemas.UserBase):
    user = await helpers.get_user(session, data)
    if not user:
        raise HTTPException(status_code=404, detail='User not found')

    if not await helpers.check_password(user, data):
        raise HTTPException(status_code=403, detail='Forbidden')

    access_token = await helpers.create_access_token(user)
    return schemas.AccessTokenResponse(access_token=access_token)
