import uvicorn
from fastapi import FastAPI

from app.auth import router as auth_router
from app.orders import router as orders_router
from app.payment import router as payment_router
from app.wallet import router as wallet_router


app = FastAPI()


app.include_router(auth_router, prefix='/auth')
app.include_router(orders_router, prefix='/orders')
app.include_router(payment_router, prefix='/payment')
app.include_router(wallet_router, prefix='/wallet')


if __name__ == "__main__":
    uvicorn.run('app.main:app', port=8000, reload=True)
