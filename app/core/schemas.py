from pydantic import BaseModel, EmailStr
from pydantic.types import UUID4


class UserData(BaseModel):
    """
        Session data, from JWT
    """
    id: UUID4
    username: str
    email: EmailStr
