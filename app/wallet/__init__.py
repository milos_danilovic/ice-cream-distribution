from fastapi import APIRouter

from . import routes

router = APIRouter()


router.add_api_route('/', routes.transaction_callback, methods=['POST'])
