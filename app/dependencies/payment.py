from httpx import AsyncClient

import orjson
from fastapi import HTTPException

from common.config import config, Config
from common.schemas.payment import (PaymentProviderReservation,
                                    TransactionReservation,
                                    ReservationWithAmount)


class PaymentGatewayAPI:
    def __init__(self, conf: Config):
        self.api_key = conf.payment_api_key
        self.api_url = conf.payment_api_url
        self.wallet_base_url = conf.callback_url

    def path(self, path):
        return f'{self.api_url}/{path}'

    async def patch_reservation_callback(self, transaction_data: ReservationWithAmount):
        return TransactionReservation(**transaction_data.model_dump(),
                                      callback=f'{self.wallet_base_url}{transaction_data.id_transaction}')

    async def reservation(self, transaction_data: ReservationWithAmount):
        transaction_data: TransactionReservation = await self.patch_reservation_callback(transaction_data)
        try:
            async with AsyncClient() as client:
                response = await client.post(self.path('charge'),
                                             headers={'X-API-Key': self.api_key},
                                             json=orjson.loads(transaction_data.model_dump_json()))
        except Exception as err:
            raise HTTPException(status_code=400, detail='Payment is currently disabled')

        if response.status_code != 200:
            # There is something wrong with payment gateway or our implementation
            raise HTTPException(status_code=400, detail='Unexpected exception (PAYERR)')

        return PaymentProviderReservation(**response.json())


payment_gateway = PaymentGatewayAPI(config())
