import asyncio
from functools import partial, lru_cache
from datetime import datetime, timedelta, UTC

import jwt

from fastapi import Request, HTTPException
from fastapi.params import Depends
from fastapi.security import HTTPAuthorizationCredentials, HTTPBearer

from pydantic import BaseModel, EmailStr

from common.db.models import master as models
from common.config import config
from common.encoders import CustomJSONEncoder


class JWTUser(BaseModel):
    sub: str
    exp: datetime
    username: str
    email: EmailStr
    user_status: models.UserStatus
    dt_created: datetime
    dt_updated: datetime | None = None


class AuthUser:
    def __init__(self, conf):
        self.secret = conf.jwt_secret
        self.algorithm = conf.jwt_algorithm
        self.jwt_type = JWTUser
        self.expire_days = conf.jwt_expire_days

    async def _verify(self, jwt_token) -> JWTUser:
        try:
            event_loop = asyncio.get_event_loop()
            data = await event_loop.run_in_executor(None,
                                                    partial(jwt.decode,
                                                            jwt_token,
                                                            key=self.secret,
                                                            algorithms=[self.algorithm]))
        except jwt.exceptions.PyJWTError as err:
            # Either invalid JWT signature or it has expired
            raise HTTPException(status_code=403, detail='Forbidden')

        return_data = self.jwt_type(**data)

        return return_data

    async def create_access_token(self, user: models.User) -> str:
        expire = datetime.now(tz=UTC) + timedelta(days=self.expire_days)

        try:
            jwt_user_data = JWTUser(sub=str(user.id),
                                    exp=expire,
                                    username=user.username,
                                    email=user.email,
                                    user_status=user.user_status,
                                    dt_created=user.dt_created,
                                    dt_updated=user.dt_updated)
        except Exception as err:
            print('Error with user data format:', err)
            raise HTTPException(status_code=403, detail='Unexpected exception')

        try:
            event_loop = asyncio.get_event_loop()
            encoded_jwt = await event_loop.run_in_executor(None,
                                                           partial(jwt.encode,
                                                                   jwt_user_data.model_dump(),
                                                                   key=self.secret,
                                                                   algorithm=self.algorithm,
                                                                   json_encoder=CustomJSONEncoder))
        except Exception as err:
            # If configuration is not set properly, this could trigger
            # TODO: Replace print with log
            print('Error while encoding JWT:', err)
            raise HTTPException(status_code=400, detail='Unexpected exception')

        return encoded_jwt

    async def create_refresh_token(self, sub: EmailStr | str) -> None:
        # TODO: Create refresh mechanism, if there is enough time
        return

    async def __call__(self,
                       request: Request,
                       token: HTTPAuthorizationCredentials = Depends(HTTPBearer())):
        return await self._verify(token.credentials)


auth = AuthUser(conf=config())
