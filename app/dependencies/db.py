from sqlalchemy.ext.asyncio import AsyncSession
from async_lru import alru_cache

from common.db.session_manager import DatabaseSessionManager
from common.config import config


conf = config()
url = f'postgresql+asyncpg://' \
      f'{conf.master_db_user}:{conf.master_db_password}' \
      f'@{conf.master_db_host}:{conf.master_db_port}' \
      f'/{conf.master_db_database}'

session_manager = DatabaseSessionManager(url)


async def session() -> AsyncSession:
    async with session_manager.session() as db_session:
        yield db_session
