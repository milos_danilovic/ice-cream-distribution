from fastapi import HTTPException, Depends
from sqlalchemy.ext.asyncio import AsyncSession

from app.dependencies.auth import auth, JWTUser

from app.dependencies import db, redis
from app.orders.helpers import get_user_cart
from app.orders.schemas import Cart

from . import helpers


async def checkout_reservation(user: JWTUser = Depends(auth),
                               session: AsyncSession = Depends(db.session),
                               redis_session: redis.Redis = Depends(redis.session)):
    cart: Cart = await get_user_cart(user, redis_session)
    if not cart.items:
        raise HTTPException(status_code=404, detail='Cart not found')
    await helpers.create_payment_reservation(user, session, redis_session, cart)
