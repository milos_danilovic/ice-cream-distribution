from typing import List
from decimal import Decimal

from app.orders.schemas import Cart, CartItem
from common.db.models.master import Currency


class OrderCartItem(CartItem):
    price: Decimal
    currency: Currency


class OrderCart(Cart):
    items: List[OrderCartItem]
