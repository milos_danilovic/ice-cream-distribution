from fastapi import APIRouter
from . import routes


router = APIRouter()


router.add_api_route('/', routes.checkout_reservation, methods=['POST'])
