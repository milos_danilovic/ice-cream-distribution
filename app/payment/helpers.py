from typing import Dict

from sqlalchemy import select
from uuid import UUID, uuid4

from fastapi import HTTPException

from app.dependencies.auth import JWTUser
from app.dependencies.payment import payment_gateway
from app.dependencies import db, redis

from app.orders.helpers import remove_cart

from common.db.models import master as models
from common.schemas.payment import TransactionReservationBase, ReservationWithAmount

from . import schemas


async def extend_cart_item(cart_item: schemas.CartItem,
                           ice_cream_map: Dict[UUID, models.IceCream]):
    order_cart_item = schemas.OrderCartItem(**cart_item.model_dump(),
                                            price=ice_cream_map[cart_item.id_ice_cream].price * cart_item.amount,
                                            currency=ice_cream_map[cart_item.id_ice_cream].currency)
    return order_cart_item


async def cart_to_transaction(user: JWTUser,
                              session: db.AsyncSession,
                              cart: schemas.Cart):
    ice_cream_ids = [ice_cream.id_ice_cream for ice_cream in cart.items]
    ice_creams_query = select(models.IceCream).where(models.IceCream.id.in_(ice_cream_ids))
    db_ice_creams = (await session.scalars(ice_creams_query)).all()

    ice_cream_map = {ic.id: ic for ic in db_ice_creams}

    order_data = schemas.OrderCart(items=[await extend_cart_item(cart_item, ice_cream_map)
                                          for cart_item in cart.items])

    return TransactionReservationBase(id_user=user.sub,
                                      id_transaction=str(uuid4()),
                                      order_data=order_data.model_dump())


async def create_order_in_db(user: JWTUser,
                             session: db.AsyncSession,
                             transaction: TransactionReservationBase):
    order_cart = schemas.OrderCart(**transaction.order_data)
    amount_to_pay = sum([item.price for item in order_cart.items])

    db_order = models.Order(id=str(transaction.id_transaction),
                            user_id=str(user.sub),
                            amount_to_pay=amount_to_pay,
                            currency=order_cart.items[0].currency,
                            status=models.OrderStatus.PENDING)

    session.add(db_order)

    db_order_items = []

    for item in order_cart.items:
        order_item = models.OrderItem(order_id=str(db_order.id),
                                      id_ice_cream=str(item.id_ice_cream),
                                      amount=item.amount)
        db_order_items.append(order_item)

    session.add_all(db_order_items)

    return ReservationWithAmount(**transaction.model_dump(),
                                 amount_to_pay=amount_to_pay)


async def create_payment_reservation(user: JWTUser,
                                     session: db.AsyncSession,
                                     redis_session: redis.Redis,
                                     cart: schemas.Cart):
    base_payment_transaction = await cart_to_transaction(user, session, cart)
    try:
        reservation_with_amount = await create_order_in_db(user, session, base_payment_transaction)
        # Even if reservation is created, and error happens while inserting in db,
        # we are not returning reservation data to frontend application
        reservation_data = await payment_gateway.reservation(reservation_with_amount)
        await session.commit()
    except Exception as err:
        await session.rollback()
        raise HTTPException(status_code=400, detail='Cannot currently create reservation')

    await remove_cart(user, redis_session)

    return reservation_data
