import asyncio
from sqlalchemy.exc import IntegrityError

from app.dependencies.db import session_manager
from common.db.models.master import IceCream, Currency
from datetime import datetime


dt_created = datetime.now()


ice_creams = [
    IceCream(id='e76fa304-5049-4c5c-8238-c00b2eee1405',
             flavor='Chocolate',
             price=2.99,
             currency=Currency.USD,
             active=True,
             dt_created=dt_created),
    IceCream(id='4820417a-ecbd-4b30-8013-331af424f72d',
             flavor='Vanilla',
             price=2.99,
             currency=Currency.USD,
             active=True,
             dt_created=dt_created),
    IceCream(id='8a572b85-6c3a-4b26-93c5-6937637321e8',
             flavor='Strawberry',
             price=2.99,
             currency=Currency.USD,
             active=True,
             dt_created=dt_created),
    IceCream(id='5e4260e3-bb00-4227-947e-94afc8e99e95',
             flavor='Stracciatella',
             price=2.99,
             currency=Currency.USD,
             active=True,
             dt_created=dt_created),

]


async def seed():
    async with session_manager.session() as sess:
        for ice_cream in ice_creams:
            try:
                sess.add(ice_cream)
                await sess.commit()
                await sess.refresh(ice_cream)
            except IntegrityError as err:
                await sess.rollback()
                continue
            except Exception as err:
                await sess.rollback()
                print(f'Error occurred while inserting ice cream {ice_cream.flavor}: {err}')
                continue

asyncio.run(seed())
