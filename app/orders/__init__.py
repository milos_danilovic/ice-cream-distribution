from fastapi import APIRouter
from . import routes


router = APIRouter()


router.add_api_route('/cart', routes.get_cart, methods=['GET'])
router.add_api_route('/cart/add', routes.add_to_cart, methods=['POST'])
router.add_api_route('/cart/remove', routes.remove_from_cart, methods=['DELETE'])
router.add_api_route('/cart', routes.remove_cart, methods=['DELETE'])
router.add_api_route('/items', routes.get_ice_creams, methods=['GET'])
