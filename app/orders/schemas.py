
from typing import List, Dict
from decimal import Decimal
from datetime import datetime

from common.schemas.base import CamelBase
from common.db.models import master as models

from pydantic.types import UUID4


class IceCream(CamelBase):
    id: UUID4
    flavor: str
    price: Decimal
    currency: models.Currency
    active: bool
    dt_created: datetime
    dt_updated: datetime | None


class IceCreamResponse(CamelBase):
    ice_creams: List[IceCream]


class CartItemBase(CamelBase):
    id_ice_cream: UUID4


class CartItem(CartItemBase):
    amount: int


class Cart(CamelBase):
    items: List[CartItem]
