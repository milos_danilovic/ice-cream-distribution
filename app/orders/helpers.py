from sqlalchemy import select
from app.dependencies.db import AsyncSession
from app.dependencies.redis import Redis, json_set, json_get
from app.dependencies.auth import JWTUser

from common.db.models import master as models

from . import schemas


async def get_ice_creams(session: AsyncSession):
    query = select(models.IceCream).where(models.IceCream.active.is_(True))
    return (await session.scalars(query)).all()


async def get_single_ice_cream(session: AsyncSession, id_ice_cream: str):
    query = select(models.IceCream).where(models.IceCream.id == id_ice_cream)
    return (await session.scalars(query)).one_or_none()


async def get_cart_key(user: JWTUser):
    return f'user_cart:{user.sub}'


async def get_user_cart(user: JWTUser,
                        redis: Redis,
                        create_if_empty: bool = False) -> schemas.Cart:
    existing_cart = await json_get(redis, await get_cart_key(user))
    existing_cart = schemas.Cart(**existing_cart) if existing_cart else None

    if not existing_cart:
        existing_cart = schemas.Cart(items=[])

        if not create_if_empty:
            return existing_cart

        await json_set(redis, await get_cart_key(user), existing_cart.model_dump())

    return existing_cart


async def save_cart(user: JWTUser,
                    redis: Redis,
                    cart: schemas.Cart):
    await json_set(redis, await get_cart_key(user), cart.model_dump())


async def add_item_to_cart(current_cart: schemas.Cart,
                           item: schemas.CartItem):
    filtered = [(idx, item)
                for idx, current_item in enumerate(current_cart.items)
                if current_item.id_ice_cream == item.id_ice_cream]

    if filtered:
        current_cart.items[filtered[0][0]].amount = item.amount
        return current_cart

    current_cart.items.append(item)

    return current_cart


async def remove_item_from_cart(current_cart: schemas.Cart,
                                item: schemas.CartItemBase):
    filtered = [(idx, item)
                for idx, cur_item in enumerate(current_cart.items)
                if cur_item.id_ice_cream == item.id_ice_cream]

    if not filtered:
        return current_cart

    current_cart.items.pop(filtered[0][0])

    return current_cart


async def remove_cart(user: JWTUser,
                      redis: Redis):
    await redis.delete(await get_cart_key(user))
