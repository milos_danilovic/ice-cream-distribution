from fastapi import Depends, HTTPException

from app.dependencies.auth import auth, JWTUser
from app.dependencies import db, redis

from . import helpers, schemas


async def get_ice_creams(session: db.AsyncSession = Depends(db.session)):
    db_ice_creams = await helpers.get_ice_creams(session)
    return schemas.IceCreamResponse(ice_creams=db_ice_creams)


async def get_cart(user: JWTUser = Depends(auth),
                   redis_session: redis.Redis = Depends(redis.session)):
    return await helpers.get_user_cart(user, redis_session)


async def add_to_cart(*,
                      session: db.AsyncSession = Depends(db.session),
                      redis_session: redis.Redis = Depends(redis.session),
                      user: JWTUser = Depends(auth),
                      item: schemas.CartItem):
    if not await helpers.get_single_ice_cream(session, item.id_ice_cream):
        raise HTTPException(status_code=404, detail='Ice cream not found')

    cart: schemas.Cart = await helpers.get_user_cart(user, redis_session, create_if_empty=True)
    cart: schemas.Cart = await helpers.add_item_to_cart(cart, item)
    await helpers.save_cart(user, redis_session, cart)

    return cart


async def remove_from_cart(*,
                           redis_session: redis.Redis = Depends(redis.session),
                           user: JWTUser = Depends(auth),
                           item: schemas.CartItemBase):
    cart: schemas.Cart = await helpers.get_user_cart(user, redis_session, create_if_empty=True)
    cart: schemas.Cart = await helpers.remove_item_from_cart(cart, item)
    await helpers.save_cart(user, redis_session, cart)

    return cart


async def remove_cart(user: JWTUser = Depends(auth),
                      redis_session: redis.Redis = Depends(redis.session)):
    await helpers.remove_cart(user, redis_session)
